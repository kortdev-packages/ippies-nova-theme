<p class="mt-8 text-center text-xs text-80">
    <a href="https://ippies.nl" class="text-primary dim no-underline">ippies.nl</a>
    <span class="px-1">&middot;</span>
    &copy; {{ date('Y') }} - By KortDev
    <span class="px-1">&middot;</span>
    Laravel Nova v{{ \Laravel\Nova\Nova::version() }}
</p>
