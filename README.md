# Ippies Nova Styling

### Installation

```bash
composer require kortdev/ippies-nova-theme
```

This theme includes adapted Nova blade files and a config file with options based on Nova Responsive Theme . To use them, first publish the config file:

```bash
php artisan vendor:publish --provider="Kortdev\IppiesNovaTheme\ThemeServiceProvider" --force
```

## Responsive
If you wan't to make Nova responsive, please run the artisan command below.
```bash
php artisan vendor:publish --provider="Gregoriohc\LaravelNovaThemeResponsive\ThemeServiceProvider"
```

Configure the options editing the `config/nova-theme-responsive.php` file.

- - -

## Credits
Responsive design is based on Nova Responsive Theme by Gregoriohc.
See https://github.com/gregoriohc/laravel-nova-theme-responsive - [Gregorio Hernández Caso](https://github.com/gregoriohc)

Copyright (c) 2020 Kortdev.

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
