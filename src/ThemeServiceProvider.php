<?php

namespace Kortdev\IppiesNovaTheme;

use Illuminate\Support\ServiceProvider;
use Laravel\Nova\Events\ServingNova;
use Laravel\Nova\Nova;

class ThemeServiceProvider extends ServiceProvider
{

    const NOVA_VIEWS_PATH =  __DIR__ . '/../resources/views/';
    const CSS_PATH =  __DIR__.'/../resources/css/';

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Nova::theme(asset('/vendor/kort-dev/ippies-theme.css'));

        Nova::serving(function (ServingNova $event) {
            Nova::style('kort-dev-theme', __DIR__.'/../resources/css/theme.css');
        });

        $this->publishes([
            self::NOVA_VIEWS_PATH => resource_path('views/vendor/nova'),
        ]);

        $this->publishes([
            self::CSS_PATH => public_path('vendor/kort-dev'),
        ], 'public');
    }
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
